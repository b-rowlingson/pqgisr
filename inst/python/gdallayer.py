import sys
from qgis.core import *
from qgis.gui import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from qgis_init import QParts

def add_gdal_layer(path, name, extent=True):
    layer = QgsRasterLayer(path, name)
    if not layer.isValid():
        raise ValueError("layer not valid")
    if extent:
        QParts.canvas.setExtent(
            QParts.canvas.mapSettings().layerToMapCoordinates(
                layer, layer.extent()
            )
        )
    QgsMapLayerRegistry.instance().addMapLayer(layer)

    QParts.canvas.refresh()

    return {'name': layer.name(), 'id': layer.id(), 'source': layer.source()}
            

def interpolate_gdal_layer(id, colours):

    lyr = QgsMapLayerRegistry.instance().mapLayer(id)

    s = QgsRasterShader()
    c = QgsColorRampShader()
    c.setColorRampType(QgsColorRampShader.INTERPOLATED)

    i = []
    for ci in range(len(colours['value'])):
        i.append(QgsColorRampShader.ColorRampItem(
            colours['value'][ci],
            QColor(colours['colour'][ci]),
            str(colours['value'][ci])
        ))

    # Now we assign the color ramp to our shader:
    c.setColorRampItemList(i)

    # Now we tell the generic raster shader to use the color ramp:
    s.setRasterShaderFunction(c)

    #Next we create a raster renderer object with the shader:
    ps = QgsSingleBandPseudoColorRenderer(lyr.dataProvider(), 1, s)
    #We assign the renderer to the raster layer:
    lyr.setRenderer(ps)
    lyr.triggerRepaint()

### to set transparency, layer.renderer().setOpacity(0 to 1), then layer.triggerRepaint()
## for vectors, layer.setLayerTransparency(0 to 100)

