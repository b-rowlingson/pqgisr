# coding=utf-8
from qgis.core import *
from qgis.gui import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *


def label_layer(id, fieldName):
    
    lyr = QgsMapLayerRegistry.instance().mapLayer(id)
    label = QgsPalLayerSettings()

    ## Now we’ll configure the labels starting with reading the current layer settings:

    label.readFromLayer(lyr)
    label.enabled = True

    ## Then we specify the attribute for the label data:

    label.fieldName = fieldName

    ## Then we can set the placement and size options:

    label.placement= QgsPalLayerSettings.AroundPoint

    label.setDataDefinedProperty(QgsPalLayerSettings.Size,True,True,"8","")

    ## And then we commit the changes to the layer:

    label.writeToLayer(lyr)

