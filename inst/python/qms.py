import quick_map_services 

from qgis._core import QgsVectorLayer
from PyQt4.QtCore import QObject, pyqtSlot, pyqtSignal, QCoreApplication, QSize
from PyQt4.QtGui import QWidget
from qgis.core import QgsMapLayerRegistry, QgsApplication, QgsVectorLayer
from qgis.gui import QgsMapCanvasLayer, QgsMapCanvas

from qgis.utils import showException, _unloadPluginModules

from qgis import utils

import logging
import sys
import time 
import traceback


def startPlugin(packageName, iface):
    print """ initialize the plugin """
    from qgis.utils import plugins, active_plugins, plugin_times

    print "testing start"
    if packageName in active_plugins:
        print "Package in active plugins"
        return False
    if packageName not in sys.modules:
        print "Package not in sys.modules"
        return False

    package = sys.modules[packageName]

    print "setting error message"
    errMsg = QCoreApplication.translate("Python", "Couldn't load plugin %s") % packageName

    start = time.clock()
    print " create an instance of the plugin"
    try:
        plugins[packageName] = package.classFactory(iface)
    except:
        _unloadPluginModules(packageName)
        msg = QCoreApplication.translate("Python", "%s due to an error when calling its classFactory() method") % errMsg
        showException(sys.exc_info()[0], sys.exc_info()[1], sys.exc_info()[2], msg, messagebar=True)
        return False

    print "# initGui"
    try:
        print "trying init gui"
        plugins[packageName].initGui()
    except:
        print "failed"
        del plugins[packageName]
        _unloadPluginModules(packageName)
        msg = "%s due to an error when calling its initGui() method" % errMsg
        traceback.print_exc()
        #showException(sys.exc_info()[0], sys.exc_info()[1], sys.exc_info()[2], msg, messagebar=False)
        return False

    # add to active plugins
    active_plugins.append(packageName)
    end = time.clock()
    plugin_times[packageName] = "{0:02f}s".format(end - start)

    return True

import os
import sys   


def add_qms(q):
    plugin_name = 'quick_map_services'

    utils.loadPlugin(plugin_name)
    startPlugin(plugin_name, q.iface) 

    dl = quick_map_services.data_sources_list.DataSourcesList()
    ds = dl.data_sources['osm_mapnik']

    quick_map_services.qgis_map_helpers.add_layer_to_map(ds)

#    print "layer registry list"
#    print QgsMapLayerRegistry.instance().mapLayers()
#    layer = QgsMapLayerRegistry.instance().mapLayers().values()[0]
#    canvas.setExtent(layer.extent())

add_qms(q)

