from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.gui import *

from qgis_init import QParts


QParts.canvas.mapRenderer().setProjectionsEnabled(True)
my_crs = QgsCoordinateReferenceSystem("epsg:3857")
QParts.canvas.mapRenderer().setDestinationCrs(my_crs)

