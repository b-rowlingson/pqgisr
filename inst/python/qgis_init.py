# coding=utf-8
"""QGIS plugin implementation.

.. note:: This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2 of the License, or
     (at your option) any later version.

.. note:: The basis of this source code was copied from the 'postgis viewer' application
     with original authors:
     Copyright (c) 2010 by Ivan Mincik, ivan.mincik@gista.sk
     Copyright (c) 2011 German Carrillo, geotux_tuxman@linuxmail.org
     Copyright (c) 2014 Tim Sutton, tim@linfiniti.com

"""

import sys

from qgis._core import QgsVectorLayer
from PyQt4.QtCore import QObject, pyqtSlot, pyqtSignal, QCoreApplication, QSize, Qt
from PyQt4.QtGui import QWidget, QMainWindow, QAction, QTextEdit, QIcon, QDockWidget, QMenu, QMessageBox
from qgis.core import QgsMapLayerRegistry, QgsApplication, QgsVectorLayer, QgsProject, QgsCoordinateReferenceSystem, QgsPluginLayer, QgsPluginLayerRegistry
from qgis.core import QgsLayerTreeModel
from qgis.gui import QgsMapCanvasLayer, QgsMapCanvas, QgsLayerTreeView, QgsLayerTreeMapCanvasBridge, QgsLayerTreeViewMenuProvider
from qgis import utils

from qgis.utils import showException, _unloadPluginModules

import time 
import traceback

import resources

class nullclass(object):
    def __getattr__(self, name):
        return nullclass()
    def __call__(self, *args):
        return nullclass()

def set_up_interface(config, args):
    """
    Sets up a QGIS pseudo-application which enables calling methods as if when calling them from QGIS-console.

    :return qgis_app: Pseudo QGIS-instance
    :rtype: QgsApplication
    :return canvas: The map canvas
    :rtype: QgsMapCanvas
    :return iface: A dummy interface, giving access to needed method-calls
    :rtype: QgisInterface
    """
    gui_flag = True  # All test will run qgis in gui mode
    qgis_app = QgsApplication(args, gui_flag)
    prefix_path = config['qgis_prefix_path']
    qgis_app.setPrefixPath(prefix_path, True)
    qgis_app.initQgis()
    QCoreApplication.setOrganizationName('QGIS')
    QCoreApplication.setApplicationName('QGIS2')


    iface = QgisInterface()

    canvas = iface.canvas

    return qgis_app, canvas, iface

#noinspection PyMethodMayBeStatic,PyPep8Naming
class QgisInterface(QObject):
    """Class to expose QGIS objects and functions to plugins.

    This class is here for enabling us to run unit tests only,
    so most methods are simply stubs.
    """
    currentLayerChanged = pyqtSignal(QgsMapCanvasLayer)

    def __init__(self):
        """Constructor
        """
        QObject.__init__(self)
        self.canvas = QgsMapCanvas()
        self.canvas.resize(QSize(400, 400))

        self.legend_interface = MyLegendInterface()
        self.active_layer = None
        # Set up slots so we can mimic the behaviour of QGIS when layers
        # are added.
        # noinspection PyArgumentList
        QgsMapLayerRegistry.instance().layersAdded.connect(self.addLayer)
        # noinspection PyArgumentList
        QgsMapLayerRegistry.instance().layerWasAdded.connect(self.addLayer)
        # noinspection PyArgumentList
        QgsMapLayerRegistry.instance().removeAll.connect(self.removeAllLayers)

        # For processing module
        self.destCrs = None


    @pyqtSlot('QgsMapLayer')
    def addLayer(self, layer):
        """Handle a layer being added to the registry so it shows up in canvas.

        :param layer: list<QgsMapLayer> list of map layers that were added

        .. note: The QgsInterface api does not include this method, it is added
                 here as a helper to facilitate testing.

        .. note: The addLayer method was deprecated in QGIS 1.8 so you should
                 not need this method much.
        """
        # set the recently added layer as active
        current_layers = self.canvas.layers()
        final_layers = [] + current_layers
        final_layers.append(QgsMapCanvasLayer(layer))
        self.canvas.setLayerSet(final_layers)
        self.active_layer = layer

    @pyqtSlot()
    def removeAllLayers(self):
        """Remove layers from the canvas before they get deleted."""
        self.canvas.setLayerSet([])

    def newProject(self):
        """Create new project."""
        # noinspection PyArgumentList
        QgsMapLayerRegistry.instance().removeAllMapLayers()

    def legendInterface(self):
        """Get the legend."""
        return self.legend_interface

    def activeLayer(self):
        """Get pointer to the active layer (layer selected in the legend)."""
        return self.active_layer

    def setActiveLayer(self, layer):
        """Set the given layer as active.
        :param layer: Layer that shall be set active
        :type layer: QgsMapLayer
        """
        self.active_layer = layer

    class actionAddFeature(object):

        def __init__(self):
            pass

        def trigger(self):
            pass

    class actionZoomToLayer(object):

        def __init__(self):
            pass

        def trigger(self):
            pass

    # ---------------- API Mock for QgsInterface follows -------------------
    def zoomFull(self):
        """Zoom to the map full extent."""
        pass

    def zoomToPrevious(self):
        """Zoom to previous view extent."""
        pass

    def zoomToNext(self):
        """Zoom to next view extent."""
        pass

    def zoomToActiveLayer(self):
        """Zoom to extent of active layer."""
        pass

    def addVectorLayer(self, path, base_name, provider_key):
        """Add a vector layer.

        :param path: Path to layer.
        :type path: str

        :param base_name: Base name for layer.
        :type base_name: str

        :param provider_key: Provider key e.g. 'ogr'
        :type provider_key: str
        """
        pass

    def addRasterLayer(self, path, base_name):
        """Add a raster layer given a raster layer file name

        :param path: Path to layer.
        :type path: str

        :param base_name: Base name for layer.
        :type base_name: str
        """
        pass

    def addToolBarIcon(self, action):
        """Add an icon to the plugins toolbar.

        :param action: Action to add to the toolbar.
        :type action: QAction
        """
        pass

    def removeToolBarIcon(self, action):
        """Remove an action (icon) from the plugin toolbar.

        :param action: Action to add to the toolbar.
        :type action: QAction
        """
        pass

    def addToolBar(self, name):
        """Add toolbar with specified name.

        :param name: Name for the toolbar.
        :type name: str
        """
        pass

    def mapCanvas(self):
        """Return a pointer to the map canvas."""
        return self.canvas

    def mainWindow(self):
        """Return a pointer to the main window.

        In case of QGIS it returns an instance of QgisApp.
        """
        return QParts.qapp
        pass


    def addDockWidget(self, area, dock_widget):
        """Add a dock widget to the main window.

        :param area: Where in the ui the dock should be placed.
        :type area:

        :param dock_widget: A dock widget to add to the UI.
        :type dock_widget: QDockWidget
        """
        pass

    def addPluginToWebMenu(self, p1, p2):
        pass

    def removePluginWebMenu(self, *args):
        pass

    def webToolBar(self):
        return nullclass()
        
    def webMenu(self):
        return nullclass()

    def messageBar(self, **argv):
        return MyMessageBar()


def eat(*args):
    pass

class MyMessageBar(object):
    def __init__(self, **argv):
        pass
    def __getattr__(self, name):
        return eat
        pass

class MyLegendInterface(object):

    def __init__(self):
        self.layer_visibility = {}

    def setLayerVisible(self, layer, yes_no):
        self.layer_visibility[layer.name()] = yes_no

    def isLayerVisible(self, layer):
        try:
            return self.layer_visibility[layer.name()]
        except KeyError:
            print('Layer {} has not been set (in)visible yet.'.format(layer.name()))
            return False


def startPlugin(packageName, iface):
    """ initialize the plugin """
    from qgis.utils import plugins, active_plugins, plugin_times

    if packageName in active_plugins:
        #print "Package in active plugins"
        return False
    if packageName not in sys.modules:
        #print "Package not in sys.modules"
        return False

    package = sys.modules[packageName]

    errMsg = QCoreApplication.translate("Python", "Couldn't load plugin %s") % packageName

    start = time.clock()
    try:
        plugins[packageName] = package.classFactory(iface)
    except:
        _unloadPluginModules(packageName)
        msg = QCoreApplication.translate("Python", "%s due to an error when calling its classFactory() method") % errMsg
        showException(sys.exc_info()[0], sys.exc_info()[1], sys.exc_info()[2], msg, messagebar=True)
        return False

    try:
        plugins[packageName].initGui()
    except:
        del plugins[packageName]
        _unloadPluginModules(packageName)
        msg = "%s due to an error when calling its initGui() method" % errMsg
        traceback.print_exc()
        #showException(sys.exc_info()[0], sys.exc_info()[1], sys.exc_info()[2], msg, messagebar=False)
        return False

    # add to active plugins
    active_plugins.append(packageName)
    end = time.clock()
    plugin_times[packageName] = "{0:02f}s".format(end - start)

    return True


class QRApp(QMainWindow):
    """ The top-level Qt App with the map canvas, layer list, and buttons """
    def __init__(self, canvas):
        super(QRApp, self).__init__()
        self.canvas = canvas
        
        self.initUI()
        
    def initUI(self):               

        dock = QDockWidget('Layers',self)

        self.addDockWidget(Qt.LeftDockWidgetArea, dock)
        dock.setWidget(QParts.view)

        self.setCentralWidget(self.canvas)

        exitAction = QAction(QIcon('exit24.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)

        self.statusBar()

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAction)

        toolbar = self.addToolBar('Exit')
        toolbar.addAction(exitAction)
        
        self.setGeometry(300, 300, 350, 250)
        self.setWindowTitle('Main window')    
        self.show()
      
from styling import Styler
  
class MyMenuProvider(QgsLayerTreeViewMenuProvider):
    def __init__(self, view):
        QgsLayerTreeViewMenuProvider.__init__(self)
        self.view = view

    def createContextMenu(self):
        if not self.view.currentLayer():
            return None
        m = QMenu()
        m.addAction("Zoom Extent", self.zoomExtent)
        m.addAction("Change Style", self.setStyle)
        return m

    def setStyle(self):
        l = self.view.currentLayer()

        if issubclass(l.__class__,QgsPluginLayer):
            QgsPluginLayerRegistry.instance().pluginLayerType(l.pluginLayerType()).showLayerProperties(l)
        else:
            canvas=QParts.canvas
            dlg = Styler(l, canvas)
            dlg.exec_()
            dlg.hide()

    def zoomExtent(self):
        layer = self.view.currentLayer()
        QParts.canvas.setExtent(
            QParts.canvas.mapSettings().layerToMapCoordinates(
                layer, layer.extent()
            )
        )
        QParts.canvas.refresh()


class QParts(object):
    @classmethod
    def init(cls, app, canvas, iface):
        cls.app=app
        cls.canvas=canvas
        cls.iface=iface
        cls.root = QgsProject.instance().layerTreeRoot()
        cls.model = QgsLayerTreeModel(cls.root)
        cls.view = QgsLayerTreeView()
        cls.view.setModel(cls.model)
        provider = MyMenuProvider(cls.view)
        cls.view.setMenuProvider(provider)
        #view.show()
        
        cls.model.setFlag(QgsLayerTreeModel.AllowNodeReorder)
        cls.model.setFlag(QgsLayerTreeModel.AllowNodeChangeVisibility)
        cls.model.setFlag(QgsLayerTreeModel.AllowLegendChangeState)
        cls.model.setFlag(QgsLayerTreeModel.ShowLegend)

        cls.bridge = QgsLayerTreeMapCanvasBridge(cls.root, cls.canvas)

import os
import sys


def init(config):
    """ initialise QParts """

    # create an app, canvas, and iface object """

    qgis_app, canvas, iface = set_up_interface(config, [])

    utils.iface = iface
    utils.plugin_paths = [os.path.expanduser(config['plugin_path'])]
    sys.path.append(os.path.expanduser(config['plugin_path']))

    #canvas.show()

    QParts.init(qgis_app, canvas, iface)

    QParts.canvas.mapRenderer().setProjectionsEnabled(True)
    my_crs = QgsCoordinateReferenceSystem("epsg:3857")
    QParts.canvas.mapRenderer().setDestinationCrs(my_crs)

    QParts.qapp = QRApp(canvas)

    # once its set up, replace the mainWindow to stop some errors
    #iface.mainWindow = QParts.qapp
