

from qgis.core import *
from qgis.gui import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from PyQt4.QtXml import QDomDocument


class Styler(QDialog):
    
    def __init__(self, layer, canvas):
        super(Styler, self).__init__()
        self.layer = layer
        self.canvas = canvas
        self.styling = QgsRendererV2PropertiesDialog(layer, QgsStyleV2.defaultStyle(), embedded=True)
        self.styling.setMapCanvas(self.canvas)
        #dlg.exec_()
        #QParts.canvas.refresh()
        self.initUI()
        
    def initUI(self):      

        self.layout = QVBoxLayout()

        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok
                                          | QDialogButtonBox.Apply);

        self.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(self.hitOk)
        self.buttonBox.button(QDialogButtonBox.Apply).clicked.connect(self.hitApply)
        
        self.layout.addWidget(self.styling)
        self.layout.addWidget(self.buttonBox)

        self.setLayout(self.layout)

        self.setWindowTitle('Style Layer')

        
    def hitApply(self):
        self.styling.apply()
        self.canvas.refresh()
    def hitOk(self):
        self.hitApply()
        self.accept()



def get_layer_style(id):
    layer = QgsMapLayerRegistry.instance().mapLayer(id)
    if not layer:
        raise ValueError,"Layer not found"

    doc=QDomDocument("xml")
    root = doc.createElement("maplayer")
    doc.appendChild(root)

    layer.writeLayerXML(root,doc,"s")
    return doc.toString()

