import sys
from qgis.core import *
from qgis.gui import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from qgis_init import QParts

def add_ogr_layer(path, name, extent=True):

    layer = QgsVectorLayer(path, name, 'ogr')

    if not layer.isValid():
        raise ValueError("layer not valid")
    if extent:
        QParts.canvas.setExtent(
            QParts.canvas.mapSettings().layerToMapCoordinates(
                layer, layer.extent()
            )
        )
    QgsMapLayerRegistry.instance().addMapLayer(layer)
    return {'name': layer.name(), 'id': layer.id(), 'source': layer.source()}


#add_ogr_layer(path, name)
