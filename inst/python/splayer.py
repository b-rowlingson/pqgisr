import sys
import base64

from qgis.core import *
from qgis.gui import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from qgis_init import QParts

typeDict = {
    "integer":QVariant.Int,
    "numeric":QVariant.Double,
    "character":QVariant.String
}

def add_sp_layer(name, the_type, projwkt, wkt, data, classes):
    """
    classes is integer|numeric|character
    """

    layer = QgsVectorLayer(the_type, name, "memory")
    if not layer.isValid():
        raise ValueError, "Layer not valid"

    crs = QgsCoordinateReferenceSystem(projwkt)
#    if crs.createFromProj4(proj4string):
    layer.setCrs(crs)
    if not crs.isValid():
        raise ValueError,"crs not valid"
    print "valid layer..."
    layer.startEditing()

    provider = layer.dataProvider()
    atts = []

    for f in range(len(classes)):
        name = data.keys()[f]
        atts.append(QgsField(name, typeDict[classes[name]]))

    provider.addAttributes(atts)

    for id in range(len(wkt)):
        feat = QgsFeature()
        geom = QgsGeometry().fromWkt(wkt[id])

        vals = []

        for ia in range(len(classes)):
            name = data.keys()[ia]
            vals.append(data[name][id])

        feat.setAttributes(vals)

        feat.setGeometry(geom)
        provider.addFeatures([feat])
    layer.commitChanges()
    layer.updateExtents()
    
    QgsMapLayerRegistry.instance().addMapLayer(layer)
    return {'name': layer.name(), 'id': layer.id(), 'source': layer.source()}


