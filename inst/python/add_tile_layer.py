# coding=utf8

import TileLayerPlugin
from TileLayerPlugin import tilelayer
from TileLayerPlugin.tiles import BoundingBox, TileLayerDefinition
from TileLayerPlugin.tilelayerplugin import TileLayerPlugin as TLP

from qgis_init import QParts

from qgis.core import QgsMapLayerRegistry

def add_tilelayer():

    bbox = None

    layerdef = TileLayerDefinition(u"OpenStreetMap",
                                   u"© OpenStreetMap contributors",
                                   "http://tile.openstreetmap.org/{z}/{x}/{y}.png",
                                   zmin=0,zmax=18,bbox=bbox)
    pl = TLP(QParts.iface)
    layer = tilelayer.TileLayer(pl, layerdef)

    QgsMapLayerRegistry.instance().addMapLayer(layer)

    return {'name': layer.name(), 'id': layer.id(), 'source': layer.source()}
