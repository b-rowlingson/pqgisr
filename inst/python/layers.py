from qgis.core import *
from qgis.gui import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from qgis_init import QParts
from styling import Styler

def layerlist():
    layers = QParts.model.rootGroup().findLayers()
    layerinfo = [(layer.layerName(), layer.layerId(), layer.layer().source(), layer.isVisible()) for layer in layers]
    return layerinfo

def remove_layers_by_name(name):
    layer_ids = [z.id() for z in QgsMapLayerRegistry.instance().mapLayersByName(name)]
    if layer_ids:
        print "removing ",layer_ids
        z = [ QgsMapLayerRegistry.instance().removeMapLayer(layer) for layer in layer_ids ]
    else:
        raise ValueError("Layer not found")

def remove_layer_by_id(id):
    QgsMapLayerRegistry.instance().removeMapLayer(id)
    

def remove_layers_by_id(layer_ids):
    print "removing ",layer_ids
    z = [QgsMapLayerRegistry.instance().removeMapLayer(id) for id in layer_ids]
    return None

def set_layer_set():
    QParts.canvas.setLayerSet([QgsMapCanvasLayer(L) for L in QgsMapLayerRegistry.instance().mapLayers().values()])

def set_extent_by_id(id):
    layer = QgsMapLayerRegistry.instance().mapLayer(id)
    if not layer:
        raise ValueError,"layer not found"
    QParts.canvas.setExtent(
        QParts.canvas.mapSettings().layerToMapCoordinates(
            layer, layer.extent()
        )
    )
    QParts.canvas.refresh()

def set_style(id):
    print "setting style for ",id
    layer = QgsMapLayerRegistry.instance().mapLayer(id)
    if not layer:
        raise ValueError,"layer not found"
        
    if issubclass(layer.__class__,QgsPluginLayer):
        print "Its a plugin layer..."
        
        QgsPluginLayerRegistry.instance().pluginLayerType(layer.pluginLayerType()).showLayerProperties(layer)
    else:
        canvas = QParts.canvas
        dlg = Styler(layer, canvas)
        dlg.exec_()
        dlg.hide()
        QParts.qapp.hide()
