require(devtools)
require(sp)


load_all("./pqgisr")

init_qgis()
tiles = add_tile_layer()
print(qgis)

g = add_gdal_layer("./Data/test.tif")
set_extent(g)

pal = data.frame(value=c(100,500,1000), colour=c("blue","white","red"))

interpolate_gdal_layer(g, colours=pal)
print(qgis)

