pqgisr
======

Experimental project to provide a QGIS map canvas for R. Something like
the following code will generate the screenshot:

```
init_qgis()

# add a shapefile directly
poly = add_ogr_layer("ukpoly.shp","poly")

# create some spatial data in R
pts = SpatialPointsDataFrame(....)

# add the SP layer object directly
# this does not save an intermediate shapefile 
ptslayer = add_sp_layer(pts,"pts")

# read some supplied data:
testpoly = raster::shapefile("testpoly.shp")

# modify attributes of testpoly...
testpoly$Resid = residuals(lm(a~b+c, data=testpoly@data))

# add to canvas
testlayer = add_sp_layer(testpoly,"testpoly")

# add basemap tiles
tiles = add_tile_layer()

set_extent(testlayer)

# show the application. Can then interactively zoom
# and pan and style the layers
qgis # or print(qgis) non-interactively

```

![Screenshot](inst/docs/images/screenshot1.png)

Right-clicking the layer in the layer list gives a menu for
zooming to the layer and opening the QGIS styling dialog.

Motivation
==========

I find I do most of my spatial analysis in R - buffering, overlay,
merging etc. I just use `rgeos` and the other spatial tools. But
I often want to load things into QGIS so that I have an interactive
visualisation of multiple layers with a rapid styling option - much
quicker than editing a script to change a colour palette and 
re-running it. So I would load data into QGIS and style it there.

But that meant jumping out of R, interrupting my workflow. Any changes 
in my statistical model meant saving as shapefiles or a SpatiaLite 
database and re-loading into QGIS. So I decided to write this package 
to provide a mapping interface for R. Now I can add spatial data
to a QGIS map directly from spatial R objects, style them and arrange 
layers and extent, all from an interactive QGIS interface.




Alpha status
============

Please note this proect, and this README file, are in a state
of constant development. Documentation is sparse and often 
out of date and incorrect. I am adding new features which might
get changed at any point. Please check the source code before
thinking there's a bug or something else wrong. Patches via 
pull requests and any suggestions via the tracker welcome!


How Does It Work?
=================

The package uses the `rPython` package to call the QGIS API. This embeds
python within R and so gives a single python context that is preserved between
calls, hence it is fast and convenient.

It works for me on Linux, it might work for you on Linux, it might even work for you
on a Mac, but I hear `rPython` support on Windows is non-existent but being worked
on.

Currently there's a few paths and configuration settings in the code that are 
specific for how I've got QGIS installed. You may need to change these if your QGIS
isn't in the same place as mine.


Operation
=========

Basic operation is to initalise, then add layers:

```
library(pqgisr)
init_qgis(...)

pts = add_ogr_layer("./Data/pts.shp")
polys = add_ogr_layer("./Data/polys.shp")
```

The application is modal, so until I figure out how to handle Qt events while R
is waiting for a prompt, when you view the map application you don't get your
prompt back. Hit the quit button to return to the R prompt.

Activate the app by printing the `qgis` object:

```
qgis # a map application appears!
```

The `qgis` object is a package global with a print method that shows the application.


Currently you can...
====================

 * Add OGR layers with `add_ogr_layer`
 * Add GDAL layers with `add_gdal_layer`
 * Remove layers with `remove_layer`
 * Add tile map layers with `add_tile_layer`
 * Add sp objects with `add_sp_layer` but only the geometry at the moment.

Adding sp-class objects
=======================

Eventually you'll be able to do this:

```
# add/remove/update sp object to map layer registry and canvas
mydata = add_sp_layer(spdata)
update_sp_layer(mydata, spdata)
update_sp_attributes(mydata, dataframe)
```

The update methods will just change the attributes and not have to retransfer the geometry
over, for speed when all you are doing is creating new data on a fixed geographic basis.


OGR and GDAL layers
===================

If you have an OGR or GDAL layer, like a shapefile or a GeoTIFF, add them:

```
foo = add_ogr_layer("./Data/foo.shp", "Foo Data")
dem = add_gdal_layer("./Data/dem.tiff")
```

Currently the extent is set to the most recent layer added.

Tile Layers
===========

These are popular choices for background context. Currently supported is
the OSM Mapnik layer using the TileMapLayer plugin. Install that first. Then you can do:

```
# add a tiled background layer
tile = add_tile_layer(name)
```

Currently this has OSM Mapnik hard-coded into itself, but should be extensible to any
tiled layer.

It might also be possible to implement layers from the `quick map services` plugin.

Basic Layer Ops
===============

Currently implemented is:

```
# get rid of layers
remove_layer(name)
# list layers
layers()
```

And TODO includes:

```
# change layer ordering
layer_order(names)

# set layer visibility
show_layer(name)
hide_layer(name)
toggle_layer(name)
```

although those operations can be done in the GUI.

App Display
===========

To show the map application, either print the package global `qgis` object, or run 
`show_map()`:

```
# run the map app
show_map()
```

Styling
=======

The style dialog will be implemented from the layer context menu in the app,
or possibly triggered by an R function. Styles will be able to be saved and loaded from
R so that R code can restore a styled set of map layers:

```
# activate the style dialog
restyle(name)
# save/load styles as XML
set_style(name, style)
get_style(name)
```

Tools
=====

The app will have a selection of map tools selectable from the app
menu or buttons - zoom/pan/query etc. 

The layer list will have context menu options for styling, setting the map 
extent, and maybe removing layers too.



