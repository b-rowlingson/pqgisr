qgisconfig <- function(){

    config = list()
    config$qgis_prefix_path="/usr"
    config$plugin_path = "~/.qgis2/python/plugins"
    config$qgis_python = "/usr/share/qgis/python"
    config$qgis_python_plugins = "/usr/share/qgis/python/plugins"
    
    return(config)
}

qgis <- new.env()
qgis$started <- FALSE
class(qgis)=c("environment","qgis")

init_qgis <- function(config = qgisconfig()){

    if(qgis$started){
        warning("qgis already started")
        return(invisible())
    }

    qgis$started <- TRUE
    
    
    rPython::python.exec("import sys")
    rPython::python.call("sys.path.append",config$qgis_python)
    rPython::python.call("sys.path.append",config$qgis_python_plugins)
    rPython::python.call("sys.path.append",python_dir())
    rPython::python.exec("import qgis_init")

    initialised = rPython::python.get("hasattr(qgis_init.QParts,'iface')")
    if(initialised){
        warning("Qgis already initialised")
        return(invisible())
    }

    rPython::python.call("qgis_init.init",config)
    
    reg.finalizer(
        qgis,
        qgis_finisher,
        onexit=TRUE)
    
    return(invisible())
    
}

end_qgis <- function(){
    if(!qgis$started){
        warning("Qgis not started according to qgis state")
        return(invisible())
    }
    qgis_imported = rPython::python.get("'qgis_init' in dir()")
    if(!qgis_imported){
        warning("qgis_init not imported")
        return(invisible())
    }
    initted = rPython::python.get("hasattr(qgis_init.QParts,'iface')")
    if(initted){
        message("calling exit")
        rPython::python.exec("qgis_init.QParts.app.exitQgis()")
        qgis$started <- FALSE
        warning("Don't try starting it again, more cleanup needed")
    }else{
        warning("QParts has no iface")
        return(invisible())
    }
}

qgis_finisher <- function(e){
    if(e$started){
        message("closing Qgis")
        initted = rPython::python.get("hasattr(qgis_init.QParts,'iface')")
        if(initted){rPython::python.exec("qgis_init.QParts.app.exitQgis()")}
    }
}

print.qgis <- function(x,...){
    if(qgis$started){
        show_canvas()
    }else{
        stop("Qgis not started")
    }
}
